__version__ = 1.0

import os.path
from re import search as re_search
import sys


def include_expand(target_file_name):
    """ replace string `#include "path/file.ext"` to content of path/file.ext """

    if not os.path.exists(target_file_name):
        print(f"File '{target_file_name}' does not exists", file=sys.stderr)
        sys.exit(1)

    input_file = open(target_file_name, "r")
    while True:
        line = input_file.readline()
        if not line:
            break

        if line.startswith("#include"):
            include_file_name = re_search(r"^#include \"(.*)\"", line).group(1)
            include_expand(include_file_name)
        else:
            print(line, end="")


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage include_expander.py input_file > output_file")
        sys.exit(0)

    targetFileName = sys.argv[1]
    include_expand(targetFileName)
